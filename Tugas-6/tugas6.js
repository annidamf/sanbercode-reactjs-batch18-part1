//soal 1
const LuasLingkaran = (r) => {
  let hasil = 3.14*r*r;
    return (hasil);
}

const KelilingLingkaran = (r) => {
    let hasil = 3.14*2*r;
  return (hasil);
}

// panggil Function
console.log(LuasLingkaran(5));
console.log(KelilingLingkaran(5));

//soal 2
let kalimat = "";

var tambahkata = (kata) => {
    kalimat += kata + " ";
return kalimat;
};

tambahkata("Saya");
tambahkata("adalah");
tambahkata("seorang");
tambahkata("frontend");
tambahkata("developer");

console.log(kalimat);

//soal 3
const newFunction = function literal(firstName, lastName){
  return {
    firstName,lastName,
    fullName: function(){
      console.log(firstName + " " + lastName)
    }
  }
}
 
//Driver Code 
newFunction("William", "Imoh").fullName() 

//soal 4
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

const {firstName, lastName, destination, occupation} = newObject;
console.log(firstName, lastName, destination, occupation)

//soal 5
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

let combined = [...west, ...east];
//Driver Code
console.log(combined);
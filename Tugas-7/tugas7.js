//soal 1
//release 0
class Animal {
    constructor(name){
      this._animalname = name;
      this._animallegs = 4;
      this._animalcold_blooded = false;
    }
  
  get AnimalName (){
    return this._animalname;
  }
  
  set Animalname(x){
    this._animalname = x;
  }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep._animalname) // "shaun"
console.log(sheep._animallegs) // 4
console.log(sheep._animalcold_blooded) // false

//release 1
class Ape extends Animal{
  constructor(name){
    super(name);
    this._animallegs = 2;
  }
  
  yell(){
    console.log("Auooo");
  }
};

var sungokong = new Ape("kera sakti");
sungokong.yell() // "Auooo"

class Frog extends Animal{
  constructor (name){
    super(name);
  }
  
  jump(){
    console.log("hop hop");
  }
};

var kodok = new Frog("buduk")
kodok.jump() // "hop hop"

//soal 2
class Clock{
  constructor({template}){
    this._template = template;
    this._timer;
  }
  
  render(){
    var date = new Date();
    var hours = date.getHours();
    
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    var output = this._template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);
  }
  
  stop(){
    clearInterval(()=>this._timer);
  }
  
  start(){
    this.render();
    this._timer = setInterval(()=>this.render, 1000);
  }
};

var clock = new Clock({template: 'h:m:s'});
clock.start(); 
//soal 1
var i = 2;
var j = 20;

console.log("LOOPING PERTAMA");
while (i<22)
  {console.log(i + " - I love coding");
  i+=2;}

console.log("LOOPING KEDUA");
while (j>0)
  {console.log(j + " - I will become a frontend developer");
  j-=2;}

//soal 2
function test(i)
{
  if (i%2==1) {
    if (i%3==0) {
      return ("I Love Coding");}
  else return ("Santai");}
  else if (i%2==0) {return ("Berkualitas");}
}

for(i=1; i<=20; i++)
  {console.log(i+ " - " + test(i))
  }

//soal 3
a = '';
var baris = 7;

for(i=1; i<=baris; i++)
  {
    a+='#'
    console.log(a)
  }

//soal 4
var kalimat="saya sangat senang belajar javascript";
var hasil= kalimat.split(" ");
console.log(hasil);

//soal 5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
daftarBuah.sort();

for(i=0; i<daftarBuah.length; i++){
console.log(daftarBuah[i]);}
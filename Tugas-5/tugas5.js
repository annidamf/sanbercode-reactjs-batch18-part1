//soal 1
function halo(){
  return ("Halo Sanbers!");
}
console.log(halo());

//soal 2
function kalikan (a,b){
  return a*b;
}

var num1=12;
var num2=4;
var hasilKali = kalikan(num1, num2);
console.log(hasilKali);

//soal 3
function introduce(name, age, address, hobby){
  return ("Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di "+address+ ", dan saya punya hobby yaitu " + hobby + "!" );
}

var name = "John"
var age = 30
var address = "Jalan belum jadi"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)

//soal 4
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992];

var PesertaObj = {
  nama : "Asep",
    jeniskelamin : "laki-laki",
      hobi : "baca buku",
        tahunlahir : 1992
}

console.log(arrayDaftarPeserta[0]);
console.log(PesertaObj.nama);

//soal 5
var buah = [
  {
  "nama" : "strawberry",
  "warna": "merah",
  "ada bijinya" : "tidak",
  "harga" : 9000 
  },
  
  {
  "nama" : "jeruk",
  "warna": "oranye",
  "ada bijinya" : "ada",
  "harga" : 8000 
  },
  
  {
  "nama" : "semangka",
  "warna": "hijau dan merah",
  "ada bijinya" : "ada",
  "harga" : 10000
  },
  
  {
  "nama" : "pisang",
  "warna": "kuning",
  "ada bijinya" : "tidak",
  "harga" : 5000
  },
]

console.log(buah[0]);

//soal 6
var dataFilm = [];

function tambahFilm(nama, durasi, genre, tahun){
  dataFilm.push({"nama" : nama, "durasi" : durasi,  "genre": genre, "tahun": tahun});
  console.log(dataFilm);
}


tambahFilm("3 Idiots", "1 jam 50 menit", "Komedi", 2007);
tambahFilm("Avengers", "2 jam", "Action", 2018);
tambahFilm("Azab", "1 jam", "Komedi", 2020);

